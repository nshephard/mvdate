## Motivation

The reason for creating this utility was to help organise the [pictures](https://flickr.com/photos/slackline/) I
take. For years I have organised my pictures on my home computer under `~/pics/YYYY/mm/dd` to facilitate finding them.
I started taking pictures before excellent programmes like [Darktable](https://www.darktable.org/) were available
that create databases of pictures and use the metadata of creation dates to allow browsing. Even though I use such tools
now I still prefer organising my pictures in this manner.
