## Installation

`mvdate` is available for installation from [PyPI](https://pypi.org). It has been developed and tested using Python 3.11
but may work with earlier versions. To install the latest release simply...

``` bash
pip install mvdate
```

If you wish to install the development version you can do so with `pip` using...

``` bash
pip install git+https://gitlab.com/nshephard/mvdate.git
```
